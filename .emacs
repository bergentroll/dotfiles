;; Add the repositories
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))
(package-initialize)

;; Fetch the list of packages available
(unless package-archive-contents
  (package-refresh-contents))

;; Use makefile-mode for Archlinux PKGBUILD-files
(add-to-list 'auto-mode-alist '("\\PKGBUILD\\'" . makefile-mode))

;; Arduino ino files support
(add-to-list 'load-path "~/.emacs.d/vendor/arduino-mode")
(setq auto-mode-alist (cons '("\\.\\(pde\\|ino\\)$" . arduino-mode)
                            auto-mode-alist))
(autoload 'arduino-mode "arduino-mode" "Arduino editing mode." t)

;; C-mode for arduino files
(add-to-list 'auto-mode-alist '("\\.ino\\'" . c-mode))

;; Use tabs in makefiles
(add-hook 'makefile-gmake-mode
          (custom-set-variables
           '(indent-tabs-mode t)))

;; Set indent for php-mode
(add-hook 'php-mode-hook
          (lambda ()
            (interactive)
            (setq c-basic-offset 2)
            (set-fill-column 120)))

;; auto-fill-mode for text buffers and disable auto-indentation
(add-hook 'text-mode-hook
          (lambda ()
            (turn-on-auto-fill)
            (electric-indent-local-mode -1)))

;; Enable bunch of indicators for file buffers
(add-hook 'after-change-major-mode-hook
          '(lambda ()
             (interactive)
             (if (buffer-file-name)
                 (progn
                   (if (require 'nlinum nil 'noerror)
                       (nlinum-mode)
                     (linum-mode))
                   (whitespace-mode)
                   (when (require 'fill-column-indicator nil 'noerror)
                     (fci-mode))))))

;; Use fill-column instead of whitespace-line-column
(setq whitespace-line-column nil)

;; Set tab stops to 4 spaces
(setq tab-stop-list (number-sequence 4 120 4))

;; Prev window keybinding
(global-set-key (kbd "C-x O") (lambda ()
                                (interactive)
                                (other-window -1)))

;; Some graphic mode tunes
(condition-case nil
    (progn
      (tool-bar-mode -1)
      (scroll-bar-mode -1)
      (font-use-system-font t))
  (error nil))

;; Maximize window
(add-to-list 'default-frame-alist
             '(fullscreen . maximized))

;; Avoid welcome buffer
(setq inhibit-startup-screen t)

;; Clear scratch buffer
(setq initial-scratch-message nil)

;; Show whitesapces and EOLs
(setq whitespace-style
      '(face newline spaces space-mark tab-mark newline-mark lines-tail))

;; Enable parentheses highlighting
(when (require 'highlight-parentheses nil 'noerror)
  (global-highlight-parentheses-mode 1))

;; Show column number near the line nubmer
(column-number-mode 1)

;; Do not ask for safe themes
(setq custom-safe-themes t)

;; Set cursor color and blinking for emacsclient
(add-to-list 'default-frame-alist '(cursor-color . "lightblue1"))
(blink-cursor-mode 1)

;;_Setted_with_emacs___________________________________________________________

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(ansi-term-color-vector
   [unspecified "#1c2023" "#c7ae95" "#95c7ae" "#aec795" "#ae95c7" "#c795ae" "#ae95c7" "#c7ccd1"] t)
 '(c-basic-offset 2)
 '(c-default-style "k&r")
 '(custom-enabled-themes (quote (wombat)))
 '(fci-rule-color "dim gray")
 '(fill-column 80)
 '(indent-tabs-mode nil)
 '(org-display-custom-times t)
 '(org-time-stamp-custom-formats (quote ("<%d/%m/%y %a>" . "<%d/%m/%y %a %H:%M>")))
 '(package-selected-packages
   (quote
    (nginx-mode php-mode lua-mode markdown-mode highlight-parentheses)))
 '(python-check-command "/usr/bin/flake8")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#cc6666")
     (40 . "#de935f")
     (60 . "#f0c674")
     (80 . "#b5bd68")
     (100 . "#8abeb7")
     (120 . "#81a2be")
     (140 . "#b294bb")
     (160 . "#cc6666")
     (180 . "#de935f")
     (200 . "#f0c674")
     (220 . "#b5bd68")
     (240 . "#8abeb7")
     (260 . "#81a2be")
     (280 . "#b294bb")
     (300 . "#cc6666")
     (320 . "#de935f")
     (340 . "#f0c674")
     (360 . "#b5bd68"))))
 '(vc-annotate-very-old-color nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum ((t (:foreground "dim gray"))))
 '(whitespace-newline ((t (:inherit whitespace-space))))
 '(whitespace-space ((t (:foreground "dim gray"))))
 '(whitespace-tab ((t (:inherit whitespace-space)))))
