# Bergentroll's must have free/open source software list

## GNU/Linux

### Media
- Ardour — digital audio studio
- Audiacity
- Calf
- Cameractrls — CLI and GUI tools to tune webcam
- Darktable
- Flameshot
- GIMP
- Gpick
- Guitarx
- HandBrake — video transcoder
- Inkscape
- Kdenlive
- LSP Plugins
- OBS Studio
- Pragha Music Player
- VidCutter
- mpv

### Documents
- GoldenDict
- Libre Office
- lowdown — MarkDown translator to HTML5, stout etc, and diff
- Meld
- Neovide
- Neovim
- Okular
- OmegaT — CAT system to help translator
- PDF Arranger
- diffpdf
- Poedit
- draw.io
- font-manager
- gImageReader
- gnome-characters
- gucharmap
- pdftricks
- python-vosk — speech-to text
- simple-scan
- whisper — speech-to text

### Network
- Remmina
- Telegram Desktop
- Thunderbird
- SysTray-X — tray plugin for Thunderbird
- X11VNC Server

### System
- Alacritty
- Apache Directory Studio
- BleachBit — cleaner
- CUPS
- Clipman
- DBeaver Community
- Fcitx 5
- File Roller
- GSmartControl
- IPython
- IRedis — user-friendly Redis CLI
- Icon Browser (from yad)
- KDiskMark
- LSHW
- Pamac — Manjaro pacman wrapper with AUR support and more
- Sigil — EPUB editor
- Tmux
- Virt Manager
- Xfce4
- [keyd](rhttps://github.com/rvaiya/keyd) — key remapper for human beings
- a2ln
- act
- bmon — network banwith monitor
- dive — explore Docker images, show diff for layers
- gnome-disk-utility
- gtk4-icon-browser (Arch Linux: `gtk4-demos`)
- kernel-modules-hook — the ArchLinux package to keep modules after kernel
- makedeb — make deb packages with PKGBUILD-formated files
- mtr — enchanted traceroute
- rua — nice AUR helper
- scrcpy
- tcpdump
- tmux-resurrect
- zfs-dkms

### Utils
- Electrum — cryptowallet
- Anki — remembering with flash cards

### C++
- clang
- GCC
- CMake
- bear
- ccls
- clangd

### AI

- ollama
- aichat

### Fonts

- Hack — DejaVu-based monospace offset
- Inter — display-adopted font set

## Android

### Documents
- Aard 2
- Editor [link](https://billthefarmer.github.io/editor/)
- LibreOffice Viewer
- Librera FD
- Markor — markdown editor and viewer
- MuPDF viewer
- Notes (`org.billthefarmer.notes_140`, [link](https://billthefarmer.github.io/notes/)) — text editor with markdown support
- QuickDic
- UnicodePad

### Network
- A2LN
- BiglyBT
- Briar — wireless serverless P2P messenger
- Ceno — anit-censorship browser with P2P access
- DAVx⁵ — CardDAV, CalDAV and more sync
- Fennec
- ICSx⁵ — sync remote ICS calendars
- NewPipe — alternative YouTube client
- NewsBlur
- Orbot — TOR service and proxy
- PixelDroid
- RadioDroid — internet radio
- Seal — download videos
- Syncthing
- Telegram
- YAACC — UPND/DLNA client and server

### System
- Disky — show sorage usage by folder
- F-Droid
- sbctl — secure boot manager, sign boot files
- Sithakuru — Sinhala keyboard
- Termux
- Unexpected Keyboard
- Wave Lines Live Wallpaper

### Misc
- AnkiDroid — remembering with flash cards
- Binary Eye
- Catima
- Electrum
- Electrum — cryptowallet (no F-Droid support yet)
- Etar — calendar app with widgets
- Fossify — collection of common phone apps, a fork of SimpleMobileTools
- FreeOTP — 2FA
- OpenKeyChain
- OsmAnd — advanced OSM-based offline maps
- Password Store [link](https://github.com/android-password-store/Android-Password-Store)
- Pocket Paint
- STARGW FX — rates of currencies
- Suntimes
- Tasks.org
- Tuner [link](https://github.com/thetwom/Tuner)
- VES - Image and Photo Compare
- VLC — advanced media player
- Weather[link](https://codeberg.org/BeoCode/Weather)
- tldr
- whoBIRD — detect birds by voice

## OpenWRT

- `luci-i18n-base-ru` — internalization package.
- `kmod-nf-nathelper` — kernel module for proper work of some apps through NAT.
  May need also `net.netfilter.nf_conntrack_helper=1` sysctl option.
- `mwan3` — multiple WAN links helper


# Misc

## Calendars

- Splendidmoons [link](https://splendidmoons.github.io) — Mahanikaya Moon
  calendar
- Prodcal [link](https://prodcal.nikitastupin.com/prodcal.ics) — Russian
  production calendar
- Sri-Lanka holidays [link](https://github.com/Dilshan-H/srilanka-holidays?tab=readme-ov-file)
