#! /bin/sh

PATH="${PATH}:${HOME}/.local/bin/"  # For pass-askpass.sh

set -e

dir=~/.ssh/sync/

warn() {
  1>&2 echo "$@"
}

files=$(find "$dir" -type f -not -name '*.pub' -not -name '*.conf')

export SSH_ASKPASS=pass-askpass.sh
export SSH_ASKPASS_REQUIRE=force

for f in $files; do
   ssh-add -vvv "$f" || warn "Failed to add $f"
done
