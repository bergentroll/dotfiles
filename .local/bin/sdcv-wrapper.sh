#!/bin/bash

ESC=$(echo -e '\e')
HL_ANT="${ESC}[31m"
HL_TRANSC="${ESC}[32m"
HL_SYN="${ESC}[33m"
HL_NUM="${ESC}[34m"
HL_ASTER=$HL_TRANSC
CLEAN="${ESC}[0m"

sed "s/<br>/\n/gi;
     s/<t>/${HL_TRANSC}[/gi;
     s/<\/.>/]${CLEAN}/g;
     s/Syn :/${HL_SYN}Syn:${CLEAN}/g;
     s/Ant :/${HL_ANT}Ant:${CLEAN}/g;
     s/\([0-9]\+)\)/${HL_NUM}\0${CLEAN}/g;
     s/\*/${HL_ASTER}*${CLEAN}/g;"
