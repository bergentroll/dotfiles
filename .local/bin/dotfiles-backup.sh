#!/bin/bash

set -e

backup_location='sources/dotfiles'
cd || exit
rsync -rav --files-from=/home/"$(whoami)"/.config/synclist \
      --exclude '*~' --exclude '#*#' \
      . $backup_location

cd $backup_location || exit
git add .

echo "Type commit message and press [ENTER]. If message is empty, \
default message will be generated:"
read -r commit_message
if [ -z "$commit_message" ]; then
    commit_message="automatically generated at $(date +'%d-%m-%Y %H:%M:%S')"
fi

git commit -m "${commit_message}"
git push origin master
