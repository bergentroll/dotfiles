#!/bin/bash

# This script generates signature for mail client on language depends on
# X11 keyboard layout.

# Check if layout group 2 is switched on
GRP2=$(xset -q | grep 'Group 2' | awk '{ print $4 }')

if [ "$GRP2" == "off" ]; then
    echo Sincerely,
    echo Anton Karmanov.
else
    echo С уважением,
    echo Антон Карманов.
fi
