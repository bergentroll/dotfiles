#! /usr/bin/env python3

# Copyright 2022 Anton Karmanov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

""" Print in wich days the next moon phase is coming

Simple script to wath the next moon day. Supposed to be used with some status
plugin e.g. Xfce genmon.

Functions takes datetime object (respecting timezone) or angle (of phase) in
radians. Example usage:

    from datetime import datetime
    import moon_phase

    date = datetime(year=2022, month=4, day=16)
    phase = moon_phase.get_phase(date)
    next_quarter_date = moon_phase.get_next_phase_date(date)
    next_phase = moon_phase.get_phase(next_quarter_date)

    print(f'Phase at {date} was {phase}.\n'
        f'Phase after that date is {next_phase} at {next_quarter_date}')

Run tests with:

    pytest moon_phase.py

Also take a look at nice ephem.next_*_moon(), ephem.prev_*_moon() functions.
"""

from datetime import datetime
from datetime import timedelta
from datetime import timezone

import ephem

try:
    import pytest
except ImportError:
    ...

PHASES = [
    '\U0001f311',
    '\U0001f312',
    '\U0001f313',
    '\U0001f314',
    '\U0001f315',
    '\U0001f316',
    '\U0001f317',
    '\U0001f318',
]
""" List of moon phases emoji"""

SYNODIC_MONTH_LENGTH = 29.53

MOON = ephem.Moon()
SUN = ephem.Sun()


def _find_moon_phase_date(
        date: datetime,
        motion: float,
        target: float) -> datetime:
    """Code is ported from ephem.__init__._find_moon_phase()"""
    def func(date_: ephem.Date) -> float:
        SUN.compute(date_)
        MOON.compute(date_)
        slon = ephem.Ecliptic(SUN).lon
        mlon = ephem.Ecliptic(MOON).lon
        return (mlon - slon - antitarget) % ephem.tau - ephem.pi

    tgt_date = ephem.Date(date)
    antitarget = target + ephem.pi
    angle = func(tgt_date)
    angle_to_cover = (- angle) % motion

    if abs(angle_to_cover) < ephem.tiny:
        angle_to_cover = motion

    phase_angle = tgt_date + SYNODIC_MONTH_LENGTH * angle_to_cover / ephem.tau
    phase_date = ephem.date(
        ephem.newton(func, phase_angle, phase_angle + ephem.hour))

    return ephem.to_timezone(phase_date, date.tzinfo)


def _get_prev_phase_angle(angle: float) -> float:
    return int(angle / ephem.halfpi) * ephem.halfpi


def _get_next_phase_angle(angle: float) -> float:
    prev_phase = _get_prev_phase_angle(angle)
    return (prev_phase + ephem.halfpi) % ephem.tau


def get_angle(date: datetime) -> float:
    """ Get moon phase angle on date in radians """
    MOON.compute(date)
    SUN.compute(date)

    return (ephem.Ecliptic(MOON).lon - ephem.Ecliptic(SUN).lon) % ephem.tau


def angle_to_phase(angle: float) -> str:
    """ Get moon phase symbol from angle """
    index = int(len(PHASES) * angle // ephem.tau)
    return PHASES[index]


def get_phase(date: datetime) -> str:
    """ Moon quarter in the given date """
    angle = get_angle(date)
    return angle_to_phase(angle)


def get_prev_phase_date(date: datetime) -> datetime:
    """ Date when current moon phase had come """

    angle = get_angle(date)
    prev_phase_angle = _get_prev_phase_angle(angle)

    return _find_moon_phase_date(date, -ephem.twopi, prev_phase_angle)


def get_next_phase_date(date: datetime) -> datetime:
    """ Date when a next moon phase will come """
    angle = get_angle(date)
    next_phase_angle = _get_next_phase_angle(angle)

    return _find_moon_phase_date(date, ephem.twopi, next_phase_angle)


def get_days_before_phase(date: datetime) -> int:
    """ In how many days a next moon phase will come

    It will be 0 if phase comes today
    """

    next_datetime = get_next_phase_date(date)
    prev_datetime = get_prev_phase_date(date)

    prev_moon_day_midnight_ = datetime.combine(
        prev_datetime, datetime.min.time())
    next_moon_day_midnight = datetime.combine(
        next_datetime, datetime.min.time())
    past_midnight = datetime.combine(date, datetime.min.time())
    days_after_phase = (past_midnight - prev_moon_day_midnight_).days
    days_before_phase = (next_moon_day_midnight - past_midnight).days

    if days_after_phase == 0:
        return 0
    return days_before_phase


def pretty_string() -> str:
    """ Make output """
    today = datetime.today()

    current_phase = get_phase(today)
    phase_in = get_days_before_phase(today)

    days_str = f'{phase_in} days' if phase_in > 0 else 'today!'

    return f'{current_phase} {days_str}'


def test_feb_2000():
    """ Test case calculates one month with shift """
    expected = [
      5, 5, 4, 3, 2, 1, 0,
      7, 6, 5, 4, 3, 2, 1, 0,
      6, 5, 4, 3, 2, 1, 0,
      6, 5, 4, 4, 3, 2, 1, 0
    ]
    date = datetime(year=2000, month=1, day=1, tzinfo=timezone.utc)
    delta = timedelta(hours=23)
    result = []
    for _ in range(30):
        days = get_days_before_phase(date)
        print(f'Date {date}, next phase in {days} days')
        date += delta
        result.append(days)
    assert result == expected, 'Some days is calculated wrong'


def test_symbols():
    assert angle_to_phase(0.0 * ephem.pi) == '\U0001f311'
    assert angle_to_phase(0.5 * ephem.pi) == '\U0001f313'
    assert angle_to_phase(1.0 * ephem.pi) == '\U0001f315'
    assert angle_to_phase(1.5 * ephem.pi) == '\U0001f317'
    assert angle_to_phase(1.999 * ephem.pi) == '\U0001f318'
    with pytest.raises(IndexError):
        assert angle_to_phase(2.0 * ephem.pi) == '\U0001f315'


if __name__ == '__main__':
    print(pretty_string())
