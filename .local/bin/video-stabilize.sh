#! /bin/bash
#
set -e

if [ -z "$1" ]; then
  echo 'Expected filename' > /dev/stderr
  exit
fi

ffmpeg_opts=('-hide_banner' '-loglevel' 'warning' '-stats')
vid=$1
name="${vid%.*}"
suf="${vid##*.}"
trans_file="/tmp/${$}_$(date +%s).trf"

vd_detect_opts='shakiness=5:accuracy=15:'  # def. 5, 15
vd_trans_opts='smoothing=8:'  # def. 10 (val * 2 + 1 frames for lowpass f.)

echo "Transform file is ${trans_file}"

ffmpeg "${ffmpeg_opts[@]}" \
  -i "$vid" -vf "vidstabdetect=${vd_detect_opts}result=${trans_file}" -f null -
ffmpeg "${ffmpeg_opts[@]}" \
  -i "$vid" -vf "vidstabtransform=${vd_trans_opts}input=${trans_file}" "${name}_s.${suf}"
ffmpeg "${ffmpeg_opts[@]}" \
  -i "$vid" -i "${name}_s.${suf}"  -filter_complex vstack "${name}_stacked.${suf}"
