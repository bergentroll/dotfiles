#! /bin/bash
set -e


args=()
ffmpeg_flags=()
help_str='
Usage: video-rotate.sh -d|--direction DIRECTION [-h|--help] [FILE]...
Rotate video to angle multiple of 90°.


  -d, --degrees\t\t90 to clockwise, 180 to upside down, 270 to counterclockwise
  -f, --force\t\tdo not ask, rewrite if output file exists
  -h, --help\t\tprint this message
'

function warn() {
  2>&1 echo "$1"
}

function err() {
  warn "$1" && exit 1
}

function set_filter() {
  # transpose arg: 0 = ccw + vflip, 1 = cw, 2 = ccw, 3 = cw + vflip
  case $1 in
    90) filter='transpose=1' ;;
    180) filter='hflip, vflip';;
    270) filter='transpose=2' ;;
    *) err "Unsupported angle $1" ;;
  esac
}

function rotate() {
  input_file="$1"
  output_file="${input_file%.*}_rot${deg}.${input_file##*.}"
  ffmpeg "${ffmpeg_flags[@]}" -i "$input_file" -vf "$filter" -c:a copy "$output_file"
}

while [[ $# -gt 0 ]]; do
  # shellcheck disable=SC2059
  case $1 in
    -d|--degrees) deg="$2" && set_filter "$deg" && shift 2 ;;
    -f|--force) ffmpeg_flags+=('-y') && shift ;;
    -h|--help) printf "$help_str" && exit 0 ;;
    -*) err "Unknown option $1" ;;
    *) args+=("$1") && shift ;;
  esac
done

if [[ -z "$deg" ]]; then
  err 'Expected valid --degrees arg'
fi

if (( ${#args[@]} == 0 )); then
  warn 'No FILE paths passed, do nothing'
else
  for f in "${args[@]}"; do
    rotate "$f"
  done
fi
