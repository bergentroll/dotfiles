#! /bin/bash

if [ -z "$1" ]; then
  echo 'Expexted filename' > /dev/stderr
  exit
fi

declare -r filename="$1"

pandoc \
  --pdf-engine=xelatex \
  --variable mainfont=Roboto \
  --variable monofont=Hack \
  --variable=colorlinks \
  --variable geometry:margin=3cm \
  "$filename" -o "${filename%.*}.pdf"
