#!/bin/bash

# Usage:
# $ site-test.sh http://example.com
# for get status once
# $ site-test.sh http://example.com 0.3
# for getting status each 0.3 seconds

site=$1

function get_status() {
    timestamp=$(date +'%d-%m-%Y %H:%M:%S')
    status=$(curl -Is "${site}" | head -1)
    echo "$timestamp $status"
}

if [ -z "$2" ]; then
    get_status
else
    while true; do
        get_status
        sleep "$2"
    done
fi
