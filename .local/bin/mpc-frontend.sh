#!/bin/bash

# Get from mpc song and artist name line.
GET_SONG_INFO () {
    mpc current
}

# Declare some variables depends on mpc status.
MAKE_STATUS_INFO () {
    # Get from mpc current mpd status.
    STATUS="$(mpc | awk 'NR==2{print $1}')"

    # Make message based on status.
    if [ "$STATUS" == "[playing]" ]; then
        SIGN="⏵"
        ICON="player_play"
    elif [ "$STATUS" == "[paused]" ]; then
        SIGN="⏸"
        ICON="player_pause"
    else
        SIGN="⏹"
        ICON="player_stop"
    fi
}

# Action based on argument.
if [ -z $1 ]; then
    MAKE_STATUS_INFO
    echo " $SIGN $(GET_SONG_INFO) "
elif [ "$1" == "toggle" ]; then
    mpc toggle
    MAKE_STATUS_INFO
    notify-send -i "$ICON" "mpd toggle" "$(GET_SONG_INFO)"
elif [ "$1" == "status" ]; then
    MAKE_STATUS_INFO
    notify-send -i "$ICON" "mpd status" "$(GET_SONG_INFO)"
else
    echo "Unknown argument $1."
fi
