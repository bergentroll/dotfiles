#! /bin/bash

conf=~/.config/qt6ct/qt6ct.conf

regex='^icon_theme=.*$'
initial_line="$(grep -e "$regex" ~/.config/qt6ct/qt6ct.conf)"
tgt_line='icon_theme=breeze-dark'

if [ "$initial_line" != "$tgt_line" ]; then
  sed -ie "s/${regex}/${tgt_line}/" "$conf"
else
  echo "Config already has ${tgt_line}, skip edit"
fi

kdenlive

if [ "$initial_line" != "$tgt_line" ]; then
  sed -ie "s/^icon_theme=breeze-dark/${initial_line}/" "$conf"
fi
