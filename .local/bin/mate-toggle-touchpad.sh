#!/bin/bash

STATE=$(dconf read /org/mate/desktop/peripherals/touchpad/touchpad-enabled)
echo $STATE

if [ "$STATE" == "true" ]; then
    dconf write /org/mate/desktop/peripherals/touchpad/touchpad-enabled false
    notify-send -i "touchpad-disabled-symbolic" -t 3000 'Touchpad disabled'
else
    dconf write /org/mate/desktop/peripherals/touchpad/touchpad-enabled true
    notify-send -i "input-touchpad-symbolic" -t 3000 'Touchpad enabled'
fi
