#! /bin/sh

set -e

askpass=/usr/lib/seahorse/ssh-askpass
path='local/ssh/'

debug() {
  1>&2 echo "$@"
}

prompt="$1"
debug "Prompt is \"$prompt\""
key=$(echo "$prompt" | sed 's%.*\/\([^/]*\):\s*$%\1%')

if [ -z "$key" ]; then
  debug 'Failed to catch key name'
  exit 1
fi

debug "Found \"$key\""

pass_out=$(pass "$path/$key" || true)

if [ -n "$pass_out" ]; then
  echo "$pass_out" | head -n1
else
  $askpass "$prompt"
fi
