#!/bin/sh

AUR_DIR=~/AUR

for DIR in $AUR_DIR/*/; do
    cd $DIR
    printf "\n[%s]\n" ${PWD##*/};
    git pull;
    cd ..;
done

printf "\n"
