#!/bin/bash

declare -r disk="$1"  # in form of sda
badblocks -t 0xaa -b 4096 -svw -o "/home/karmanov/${disk}.log" "/dev/${disk}"
