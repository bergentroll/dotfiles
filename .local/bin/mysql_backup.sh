#!/bin/sh

# Set variables
path="/home/anton/database_backup/"
mkdir -p $path
timestamp=$(date -I)

# Make dump
mysqldump --login-path=local -A | gzip -c > "${path}${timestamp}.gz"

# Cut history to 5 lates backups
cd "${path}" || exit
rm $(ls | head -n -5) 2> /dev/null
