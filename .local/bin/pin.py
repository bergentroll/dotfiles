#! /usr/bin/env python3

import sys
import random

DEFAULT_LENGTH = 4


def generate_pin(length) -> str:
    val = random.randrange(0, 10 ** length - 1)
    return str(val).zfill(length)


if __name__ == '__main__':
    try:
        length = int(sys.argv[1])
    except IndexError:
        length = DEFAULT_LENGTH

    print(generate_pin(length))
