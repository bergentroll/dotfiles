#! /usr/bin/env python3

"""
2023 Anton Karmanov <a.karmanov@inventati.org> CC-0

This script helps to reuse Kdenlive video editor guides as labels in Audacity
audio editor.

To use follow the steps:
1. Open a project with guides int Kdenlive. Guide may be added with a context
   menu on the timeline
2. Find the "Timeline Guides" frame (look at main menu -> View -> Guides)
3. Press the menu button on the frame and click then "Export..."
4. Change format to "{{frame}} {{comment}}"
5. Export guides to a file
6. Check framerate of the project at main menu -> Project -> Project Settings
7. Run this script like following:
   python3 SCRIPT.py -s EXPORTED.txt -d DESTINATION.txt -f FRAMERATE
8. Import created DESTINATION_FILE.txt in Audacity with main menu -> Edit ->
   Labels -> Label Editor
"""


import argparse


def _get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='')
    parser.add_argument(
        '-s', '--source',
        required=True,
        type=str,
        help='Text file of Kdenlive guides of format "{{frame}} {{comment}}"')
    parser.add_argument(
        '-d', '--destination',
        required=True,
        type=str,
        help='Audiacity labels file to create')
    parser.add_argument(
        '-f', '--framerate',
        required=True,
        type=float,
        help='Framerate of a Kdenlive project where guides coming from')
    return parser.parse_args()


class KdenliveGuide:
    def __init__(self, frame: int, label='', framerate=30.0) -> None:
        self.frame = frame
        self.label = label
        self.framerate = framerate

    @property
    def second(self) -> float:
        return self.frame / self.framerate

    def __str__(self) -> str:
        return f'{self.frame} {self.label}'


class AudacityGuide:
    def __init__(self, start: float, end: float, label='') -> None:
        self.start = start
        self.end = end
        self.label = label

    @staticmethod
    def from_kdenlive_guide(obj: KdenliveGuide) -> 'AudacityGuide':
        timecode = obj.second
        new_obj = AudacityGuide(start=timecode, end=timecode, label=obj.label)
        return new_obj

    def __str__(self) -> str:
        return f'{self.start}\t{self.end}\t{self.label}'


def kdenlive_to_audacity_guides(
    source: str, dest: str, framerate: float
) -> None:
    data = []
    with open(source, 'r') as file:
        for line in file.read().splitlines():
            frame_str, label = line.split(' ', 1)
            frame = int(frame_str)
            guide = KdenliveGuide(
                frame=frame,
                label=label,
                framerate=framerate)
            data.append(guide)

    with open(dest, 'w') as file:
        for guide in data:
            new_guide = AudacityGuide.from_kdenlive_guide(guide)
            file.write(f'{new_guide}\n')


if __name__ == '__main__':
    args = _get_args()
    kdenlive_to_audacity_guides(
        source=args.source,
        dest=args.destination,
        framerate=args.framerate)
