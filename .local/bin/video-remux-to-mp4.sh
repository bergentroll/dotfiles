#! /bin/bash

set -e

if [ -z "$1" ]; then
  echo 'Expeted filename' > /dev/stderr
  exit
fi

command -V ffmpeg 

declare -r filename="$1"

ffmpeg -i "$filename" -codec copy "${filename%.*}.mp4"
