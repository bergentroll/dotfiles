#!/bin/bash

function list_with_size {
  for line in $(pacman -Qlq "$1"); do
    if [[ -f "$line" ]]; then du -h "$line"; fi
  done
}

for pkg in "$@"; do
  list_with_size "$pkg"
done
