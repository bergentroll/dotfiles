import winrm
import colorama


class WindowsRunner:
    """
    Run command on remote Windows host

    Simple helper provides methods to get a single file by HTTP and run it with
    custom command. It may be useful to debug some script.

    python -m http.server is recommended for carrying files.
    """
    def __init__(self, win_host: str, login='admin', password='password'):
        self.session = winrm.Session(win_host, auth=(login, password))
        self.std_out = ''
        self.std_err = ''

    def _get(self, url: str, save_as=r'C:\file_to_run'):
        res = self.session.run_ps(
            '(new-object System.Net.WebClient)'
            f'.DownloadFile("{url}", "{save_as}")')
        self.std_out = res.std_out.decode("cp1251")
        self.std_err = res.std_err.decode("cp1251")

    def _run(self, command: str):
        cmd_list = command.split()
        res = self.session.run_cmd(cmd_list[0], cmd_list[1:])
        self.std_out = res.std_out.decode("cp1251")
        self.std_err = res.std_err.decode("cp1251")

    def _print_out(self):
        print(colorama.Fore.GREEN + self.std_out + colorama.Style.RESET_ALL)
        print(colorama.Fore.RED + self.std_err + colorama.Style.RESET_ALL)

    def get_and_run(self, url, save_as=r'C:\file_to_run', command=r'C:\file_to_run'):
        self._get(url, save_as)
        self._print_out()
        self._run(command)
        self._print_out()

    def get_and_run_py(self, url):
        self._get(url, save_as=r'C:\file.py')
        self._print_out()
        self._run(r'C:\salt\bin\python.exe C:\file.py')
        self._print_out()


if __name__ == '__main__':
    ...
