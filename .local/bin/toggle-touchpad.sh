#!/bin/bash

name='SynPS/2 Synaptics TouchPad'

state=$(xinput list-props "$name" | grep "Device Enabled"\
     | grep -o ".$")

if [ $state = '0' ] && [ -z $1 ]; then
    xinput enable "$name"
    notify-send -t 3000 'Touchpad enabled'
else
    xinput disable "$name"
    notify-send -t 3000 'Touchpad disabled'
fi
