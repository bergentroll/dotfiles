#! /bin/bash

declare -r mode="1168x540_60.00"
port="$1"

if [ -z "$port" ]; then
  port=5900
fi

adb reverse "tcp:$port" "tcp:$port"

xrandr --newmode "$mode" 49.37  1168 1200 1320 1472  540 541 544 559  -HSync +Vsync
xrandr --addmode HDMI-1 "$mode"
xrandr --output HDMI-1 --mode "$mode" --right-of DVI-D-1

x11vnc -rfbport "$port" -clip 1168x540+1920+0

xrandr --output HDMI-1 --off
