#! /bin/bash
set -e

command -V ffmpeg

if [ -z "$1" ]; then
  echo 'Expected filename' > /dev/stderr
  exit
fi

declare -r filename="$1"
declare -ir crf="${2:-25}"

ffmpeg -i "$filename" -v warning -stats \
  -acodec copy -vcodec h264 -crf "$crf" \
  "${filename%.*}_crf${crf}.${filename##*.}"
