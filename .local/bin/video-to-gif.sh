#! /bin/bash

input_file=$1
to=$3

from_arg=(-ss "${2:-00:00:00}")

if [[ -n $to ]]; then
  to_arg=(-to "$to")
fi

ffmpeg -i "$input_file" -r 10 -vf 'scale=300:-1' -an "${from_arg[@]}" "${to_arg[@]}" output.gif
