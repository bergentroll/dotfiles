#!/bin/bash
set -e

# Thx https://www.reddit.com/r/xfce/comments/4y8zld/comment/d6pay2x/
#
# Usage: script.sh <panel id>
 
channel=xfce4-panel
panel_id=$1
if [[ -z "$panel_id" ]]; then
   echo 'Neetds valid panel ID as arg' && exit 1
fi

prop="/panels/panel-${panel_id}/autohide-behavior"
cur=$(xfconf-query -c "$channel" -p "$prop")

if [[ $cur == 0 ]]; then
    echo Set to autohide
    xfconf-query -c "$channel" -n -p "$prop" -t int -s 2
else
    echo Set to not hide
    xfconf-query -c "$channel" -n -p "$prop" -t int -s 0
fi
