#! /bin/bash

camera_id="${1:-1}"
device='/dev/video22'

echo "Camera id ${camera_id} selected"
echo

if [ ! -c "$device" ]; then
  pkexec modprobe v4l2loopback \
    devices=1 video_nr=22 exclusive_caps=1 card_label='Virtual Webcam'
fi

scrcpy \
  --max-size 1920 \
  --v4l2-sink="$device" \
  --no-playback \
  --video-source=camera \
  --camera-id="$camera_id"
