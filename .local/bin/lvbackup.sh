#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

TIMESTAMP=$(date +'%d-%m-%Y_%H:%M:%S')
ROOT_SIZE=$(df -B1 /dev/vg0/root | tail -n1 | awk '{ print $2 }')
BOOT_SIZE=$(df -B1 /dev/vg0/boot | tail -n1 | awk '{ print $2 }')

lvcreate -L10G -s -n rootsnapshot /dev/vg0/root
dd if=/dev/vg0/rootsnapshot | pv -s ${ROOT_SIZE} | gzip > /nfs/backups/cloud/rootsnapshot-${TIMESTAMP}.img.gz
lvremove -f /dev/vg0/rootsnapshot

dd if=/dev/vg0/boot | pv -s ${BOOT_SIZE} | gzip > /nfs/backups/cloud/boot-${TIMESTAMP}.img.gz

printf "__________________________________\nBackup has been created.\n"
