#!/bin/bash

# Set delimiter
IFS=$'\n'

# Make dirs
while read -r name; do
    dir=${name%.*}
    mkdir -v "${dir}"
    unzip "${name}" -d "${dir}"
    rm -f "$name"
done <<< "$(find ./ -iname '*.zip' -type f)"
