#!/bin/bash

# Tune notification options
app_name=$(basename "$0")
notif_options="--app-name=$app_name --expire-time=1500"
# Do not keep notification in history
notif_options="$notif_options --hint=byte:transient:True"

# Get corresponding propery names for all pointers
mapfile -t properties < <(xfconf-query -c pointers -l | grep 'Device_Enabled')

function toggle_touchpad {
  # Get and print current device state (bool)
  state=$(xfconf-query -c pointers -p "${1}")

  # Toggle device state based on current state and send notification
  if [ "$state" == "1" ]; then
    xfconf-query -c pointers -p "${1}" -s 0
    notify-send "$notif_options" \
      --icon "touchpad-disabled-symbolic" 'Touchpad disabled'
        else
          xfconf-query -c pointers -p "${1}" -s 1
          notify-send "$notif_options" \
            --icon "input-touchpad-symbolic" 'Touchpad enabled'
  fi
}

for prop in "${properties[@]}"; do
  toggle_touchpad "$prop"
done
