#!/bin/sh
username="$(whoami)"
echo "${username} ALL=(ALL) NOPASSWD:ALL" |
  sudo tee "/etc/sudoers.d/${username}"
