#!/bin/bash

# If not running interactively, then do nothing.
if [[ $- != *i* ]]; then
  return
fi

# Include additional configs.
while read -r file; do
  if [ -z "$file" ]; then continue; fi
  # shellcheck source=/dev/null
  source "$file"
done <<< "$(find ~/.bashrc.d/ \( -type f -o -type l \) -iname '*.sh' | sort)"

# Export variables.
export PATH="$PATH:${HOME}/.local/bin/"
export HISTCONTROL="ignoreboth:erasedups"
export HISTSIZE=10000
export HISTFILESIZE=100000

# Ignore ctrl-d.
set -o ignoreeof

# Make > and >> more safe.
set -o noclobber
