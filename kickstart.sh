#!/bin/bash

path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"

mkdir ~/.config/

ln -sf "${path}/.bashrc" ~/
ln -sf "${path}/.bashrc.d/" ~/

ln -sf "${path}/.tmux.conf" ~/

## DEPRECATED
#ln -sf "${path}/.Xresources" ~/
#ln -sf "${path}/.inputrc" ~/

mkdir ~/.gnupg/
ln -sf "${path}/.gnupg/gpg-agent.conf" ~/.gnupg/

ln -sf "${path}/.vimrc" ~/
ln -sf "${path}/.vim/" ~/

ln -sf "${path}/.gitconfig" "${path}/.gitexcludes" "${path}/.git_template/" ~/

## Scipts
mkdir -p ~/.local/bin/
ln -sf "${path}/.local/bin/nopasswd.sh" ~/.local/bin/
ln -sf "${path}/.local/bin/video*.sh" ~/.local/bin/
ln -sf "${path}/.local/bin/pin.py" ~/.local/bin/

ln -sf "${path}/.pylintrc" ~/

ln -sf "${path}/.config/alacritty/" ~/.config/

ln -sf "${path}/.config/feh/" ~/.config/

ln -sf "${path}/.config/nvim/" ~/.config/

ln -sf "${path}/.config/ripgreprc" ~/.config/

mkdir -p ~/.config/mpv/
ln -sf "${path}/.config/mpv/mpv.conf" ~/.config/mpv/
ln -sf "${path}/.config/mpv/script.opts" ~/.config/mpv/

mkdir -p ~/.config/fontconfig/
ln -sf "${path}/.config/fontconfig/fonts.conf" ~/.config/fontconfig/

## DEPRECATED
#mkdir -p ~/.devilspie/
#ln -sf "${path}/.devilspie/general.ds" ~/.devilspie/

mkdir -p ~/.ssh/
if [[ -f ~/.ssh/config ]]; then
  echo File already exists: ~/.ssh/config
else
  cp "${path}/.ssh/config" ~/.ssh/config
fi
