" Source aux config files
for file in split(globpath('~/.vim/vimrc.d/', '*'), '\n')
  if fnamemodify(file, ':e') == 'vim'
    exec 'source' file
  endif
endfor

" Restrict options from outer files
set secure

" Enable modeline
set modeline modelines=5

" Solid window borders
set fillchars+=stl:─,stlnc:╌,vert:│

" Tune GVIM
if has('gui_running')
  set guioptions-=L
  set guioptions-=T
  set guioptions-=r
endif

" Ignore case on search
set ignorecase smartcase

" Highlight every search match
set hlsearch

" Completion options
set completeopt=menu,menuone,noselect,noinsert
if !has('nvim')
  set completeopt+=popup
endif
set complete-=i
set complete+=kspell

set formatoptions=cq

" Langs for spellchecking
set nospell spelllang=ru,en

" Search for file recursively
set path+=**

" Command mode completion
set wildmenu wildmode=longest,list,full

" Always display status line
set laststatus=2

" Control presence of entries in session files
set sessionoptions-=fold

" Save buffer automatically when switch to other buffer
set autowrite

" Allow hidden buffers to keep undo history on buffers switching
set hidden

" Length of history
set history=1000

" Switch to tab with requested buffer if already opened, new window for
" quickfix command
set switchbuf=usetab,split

" Recognize file types
filetype on
filetype plugin on
filetype indent on

" Show lines numbers
set number

" Go throught eol
set whichwrap=b,s,<,>,[,]

" Do not wrap searching
set nowrapscan

" Default textwidth
set textwidth=79

" 80 rule
set colorcolumn=+1

" Identation settings
set tabstop=2 shiftwidth=2 smarttab

" Tune C++ indentation
set cinoptions=g0,i-s cinkeys-=0#

" Spaces instead of tabs
set expandtab

" Show whitespaces
set list
silent! set listchars=eol:$,tab:→\ ,space:·,nbsp:↔

" Addition layout
set keymap=russian-jcukenwin iminsert=0 imsearch=0

" Mouse support
set mouse=a

" Set window (GUI) title with titlestring 
set title

" Keep cursor position on quit
augroup resCur
  autocmd!
  autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END

" Track terminal resizing
autocmd VimResized * exe "normal \<c-w>="

" Hardcopy setup
if !has('nvim')
  set printencoding=koi8-r
endif
