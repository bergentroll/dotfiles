# dotfiles
## Overview
This traditional repo contains my config files set.

## Fast start

`bootstrap.sh` script installs few main configs to `$HOME`. It may rewrite
existing files! Script creates symlinks to the repo to avoid synchronization
routines.

## Bash config

Bash config is modular. The main file is `.bashrc`. It sources any `*.sh`
inside the `~/.bashrc.d/` (including nested directories). `.bashrc.d/local/` is
ignored for this repo for cases when `~/.bashrc.d/` is a symlink.

Rich status string configured at the `.bashrc.d/11-ps1_update.sh`. It looks
like:
```
┌user@hostname:~/current/working/dir
│DATE: Sun Mar 01 2021 12:00:05 MSK
│JOBS: [1+ vim] [2- ] 
└$ 
```

## Vim config

Neovim is my main editor flavour today. Config is still backward compatible
with Vim. Vim config is modular. The main file is `.vimrc` which sources any
`~/.vim/vimrc.d/*.vim` files. All Neovim-specific stuff is in `.config/nvim/`
with Lua as a preffered syntax.

There is a list of favorit plugins, names given with github.com namespace when
applicable:

- `catppuccin/nvim` — nice colorscheme with terminals and GTK versions also
- `vim-airline/vim-airline-themes`
- `vim-airline/vim-airline` — nice status bar
- `nanotech/jellybeans.vim` — eye-candy colorscheme, a bit outdated thought

- `neovim-lspconfig` — makes easy to use language protocol servers with Neovim
- `dense-analysis/ale` — powerful linters and code analysers wrapper
- `sheerun/vim-polyglot` — collections of improved syntax files

- `preservim/nerdtree` — a bit stale but useful file manager
- `preservim/tagbar` — tags navigation pane
