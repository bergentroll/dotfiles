#!/bin/bash

# If job entry is longer, it will be cutted
JOB_MAX_LENGTH=24

# Set prompt colors
if [ "$USER" = "root" ] ; then
  USER_COLOR=${COLORS[red]};
else
  USER_COLOR=${COLORS[green]};
fi

# Set host string color for ssh and mc
if [ -n "${MC_SID+x}" ] || [ -n "${SSH_CLIENT+x}" ]; then
  HOST_COLOR=${COLORS[yellow]};
else
  HOST_COLOR=${COLORS[green]};
fi

function update_chroot() {
  local tmp_chroot="${CHROOT}"
  if [ -n "${VIRTUAL_ENV}" ]; then
    tmp_chroot="${tmp_chroot}${COLORS[dark_gray]}(python-venv)"
  fi
  echo "${tmp_chroot}"
}

function cut_long_line() {
  local line="$1"
  if (( ${#1} >= ${2} )); then
    line="${1::$2}…"
  fi
  echo -n "$line"
}

# Make cute line with fg jobs for PS1
function jobs_line() {
  local job pid job_id line
  while read -r job; do
    # Clear unwanted lines of multiline jobs
    job=$(echo "$job" | grep '^\[[0-9]')
    if [ -z "$job" ]; then
      continue
    fi
    # Cut status and brakets
    pid=$(echo "$job" | awk '{ print $2 }')
    job_id=$(echo "$job" | awk '{ print $1 }' | sed -e 's/\[//' -e 's/\]//')
    job_cmd=$(ps -o cmd --no-headers -p "$pid")
    line="$job_id $job_cmd"
    line=$(cut_long_line "${line}" ${JOB_MAX_LENGTH})
    echo -n "[${line}] "
  done <<< "$(jobs -l)"
}

# Dynamic PS1 magic!
function update_ps1() {
  PS1=""
  PS1="$PS1┌$(update_chroot)"
  PS1="$PS1$USER_COLOR\u${COLORS[END]}@$HOST_COLOR\h${COLORS[END]}"
  PS1="$PS1:${COLORS[blue]}\w${COLORS[END]}\n"
  local jobs_line
  jobs_line=$(jobs_line)
  if [ -n "$jobs_line" ]; then
    PS1="$PS1│${COLORS[dark_gray]}JOBS: $(jobs_line)${COLORS[END]}\n"
  fi
  PS1="$PS1└\$ "

  # Set terminal title
  printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"
}

export PROMPT_COMMAND="update_ps1"
