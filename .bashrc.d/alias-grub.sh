alias arch-grub-install='sudo grub-install --target=x86_64-efi --efi-directory=/boot/ --bootloader-id=GRUB'
alias arch-grub-mkconfig='sudo grub-mkconfig -o /boot/grub/grub.cfg'
