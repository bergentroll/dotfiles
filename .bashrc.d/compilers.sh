## ccache
export PATH="/usr/lib/ccache:/usr/lib/ccache/bin:$PATH"

## CMake
export CMAKE_EXPORT_COMPILE_COMMANDS='ON'

## Make
THREADS_NUM=$(($(nproc) + 1))
export MAKEFLAGS="--jobs $THREADS_NUM"

## GCC
common_flags="-fdiagnostics-color=always -Wall -Wfatal-errors"
export CFLAGS="$common_flags -Wstrict-prototypes"
export CXXFLAGS="$common_flags -Woverloaded-virtual"

## Golang
export GOPATH="$HOME/.go"
