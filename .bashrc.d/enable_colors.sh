alias diff='diff --color'

alias grep='grep --color'

alias ls='ls --color=auto'
eval "$(dircolors ~/.config/dircolors 2> /dev/null || dircolors)"

man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;32m' \
    LESS_TERMCAP_md=$'\e[01;32m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;34m' \
    man "$@"
}
