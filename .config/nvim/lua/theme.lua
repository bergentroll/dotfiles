local ok, catppuccin = pcall(require, 'catppuccin')
if not ok then
  print('No Catppuccin theme found')
  return
end

if not os.getenv("DISPLAY") then
  vim.cmd.colorscheme('default')
  vim.cmd.highlight({'StatusLine', 'ctermbg=darkblue'})
  vim.cmd.highlight({'SpecialKey', 'ctermfg=darkblue'})
  vim.cmd.highlight({'Whitespace', 'ctermfg=darkblue'})
  vim.cmd.highlight({
    'ColorColumn',
    'ctermbg=DarkGrey',
    'ctermfg=Black',
    'guibg=DarkGrey',
    'guifg=Black'
  })
else
  catppuccin.setup{
    integrations = {
      native_lsp = {
        enabled = true,
        virtual_text = {
          errors = { 'italic' },
          hints = { 'italic' },
          warnings = { 'italic' },
          information = { 'italic' },
        },
        underlines = {
          errors = { nil },
          hints = { nil },
          warnings = { nil },
          information = { nil },
        },
        inlay_hints = {
          background = true,
        },
      },
    },
  }

  vim.cmd.colorscheme('catppuccin')
  vim.g.airline_theme = 'catppuccin'
end

vim.o.cursorline = true
