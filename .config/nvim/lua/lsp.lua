-- Setup language servers
-- For server specific options see:
-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
local ok, lspconfig = pcall(require, 'lspconfig')
if not ok then
  print('No lspconfig plugin found')
  return
end

lspconfig.bashls.setup{ }
lspconfig.clangd.setup{ }
lspconfig.dockerls.setup{ }
lspconfig.jsonls.setup{ }
lspconfig.lua_ls.setup{
  settings = {
    Lua = {
      runtime = { version = 'LuaJIT' },
      diagnostics = {
        globals = {
          'vim',
          ---'require'
        },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file('', true),
      },
    },
  },
}
lspconfig.pkgbuild_language_server.setup{ }
lspconfig.pylsp.setup{
  settings = {
    pylsp = {
      configurationSources = { 'flake8' },
      plugins = {
        mypy = {
          enable = true,
          overrides = { '--check-untyped-defs' },
        },
        flake8 = { enabled = true },
        mccabe = { enabled = false },  -- already in flake8
        pycodestyle = { enabled = false },  -- already in flake8
        pyflakes = { enabled = false },  -- already in flake8
        pylint = { enabled = false },
        ruff = { enabled = false },
      }
    }
  }
}
--lspconfig.pyright.setup{ }
lspconfig.rust_analyzer.setup{
  -- Server-specific settings. See `:help lspconfig-setup`
  settings = {
    ['rust-analyzer'] = { },
  },
}
lspconfig.ts_ls.setup{ }
lspconfig.vimls.setup{ }
lspconfig.yamlls.setup{ }

-- LaTeX
lspconfig.digestif.setup{ }
lspconfig.texlab.setup{ }

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', { }),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})

vim.keymap.set('n', '<F5>', ':LspRestart<cr>')

-- Diagnostic message format
vim.diagnostic.config({
  virtual_text = true,
  float = {
    show_header = true,
    format = function(diagnostic)
      return string.format(
        '%s\n\nsource: %s, ncode: cd%s',
        diagnostic.message,
        diagnostic.source,
        diagnostic.code)
    end,
  },
})
