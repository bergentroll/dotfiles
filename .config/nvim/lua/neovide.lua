require('guifont')
vim.o.guifont = guifont
vim.g.neovide_hide_mouse_when_typing = true
vim.g.neovide_cursor_trail_size = 0.5
vim.g.neovide_cursor_animate_command_line = true
vim.g.neovide_cursor_vfx_mode = 'railgun'
vim.g.neovide_underline_stroke_scale = 0.1
