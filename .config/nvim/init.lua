--- Vim compatibility
vim.opt.runtimepath:prepend('~/.vim')
vim.opt.runtimepath:append('~/.vim/after')
vim.o.packpath = vim.o.runtimepath
vim.cmd 'source ~/.vimrc'

--- Config modules
require('theme')
require('lsp')
  require('telescope_rc')
if vim.g.neovide then
  require('neovide')
end

--- Main settings
vim.o.signcolumn='yes'
vim.g.ale_enabled=0
