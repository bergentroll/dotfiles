#! /bin/bash

echo input-keyboard-symbolic
layout=$(hyprctl devices -j | jq -r '.keyboards[] | select(.main == true) | .active_keymap')
echo "$layout"| awk '{print $1;}'
