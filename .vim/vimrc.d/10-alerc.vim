let g:ale_virtualtext_cursor = 'current'

" Completion settings
let g:ale_completion_enabled = 1
let g:ale_completion_delay = 50

if has('nvim')
  let g:ale_use_neovim_diagnostics_api = 1
endif

" Always show ALE sing gutter
let g:ale_sign_column_always = 1

let g:ale_echo_msg_format = '%linter%%[code]%: %s'

" Restrict enabled linters lists
let g:ale_linters = {}
let g:ale_linters['cpp'] = ['clangd', 'clangtidy', 'cppcheck', 'cpplint', 'cquery', 'flawfinder']
"let g:ale_linters['python'] = ['flake8', 'pylint', 'mypy', 'pylsp']
let g:ale_linters['python'] = ['ruff', 'flake8', 'mypy']

" To avoid missing c++ std headers.
let g:ale_cpp_clangcheck_extra_options = '-- -Wall -x c++'
let g:ale_cpp_clangtidy_extra_options = '--extra-arg=-Wall -x c++'

" For case when ghc-static is not presented.
let g:ale_haskell_ghc_options = '-dynamic -fno-code -v0'

" Ignore 'ugly table line' warning.
let g:ale_tex_chktex_options = '-I -n 44'

" Ignore import troubles like missing stubs.
let g:ale_python_mypy_options = '--ignore-missing-imports'

" Run Pylint in parallel
let g:ale_python_pylint_options = '--jobs 0'

" Colors
highlight ALEVirtualTextError gui=italic guibg=NONE guifg=darkyellow
highlight ALEVirtualTextError cterm=italic ctermbg=none ctermfg=darkyellow
highlight link ALEVirtualTextWarning ALEVirtualTextError
highlight link ALEVirtualTextInfo ALEVirtualTextError

" Disable inline message
"let g:LanguageClient_useVirtualText = 0
