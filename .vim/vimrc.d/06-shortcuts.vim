" Close buffer but keep window.
nmap <F4> :bnext<CR>:bdelete #<CR>

" Quick save
noremap <Leader>w :wall<CR>
" Reload config
noremap <Leader>r :source $MYVIMRC<CR>
" Sort list
vmap <Leader>s s<c-r>=join(sort(split(@", '\s*,\s*')), ', ')<cr><esc>
" Update tags
map <Leader>t :!ctags --recurse --verbose<CR>


" NERDTree keybindings
nmap <leader>n :NERDTreeFocus<CR>
nmap <esc>t :NERDTreeToggle<CR>
nmap <esc>f :NERDTreeFind<CR>

" ALE keybinds
if !has('nvim')
  nmap <C-j> <Plug>(ale_next)
  nmap <C-k> <Plug>(ale_previous)
  nmap <C-n> <Plug>(ale_next_error)
  nmap <C-p> <Plug>(ale_previous_error)
  nmap <F5> :ALEReset<CR>:ALELint<CR>
endif

" Tagbar keybindings
nmap <F8> :TagbarToggle<CR>
