" Use syntax files.
syntax on

" Set colorscheme.
if !has('nvim')
  colorscheme default
  highlight StatusLine ctermbg=darkblue
  highlight SpecialKey ctermfg=darkblue
  highlight Whitespace ctermfg=darkblue
  hi ColorColumn ctermbg=DarkGrey ctermfg=Black guibg=DarkGrey guifg=Black
endif

" vim-airline fine-tuning.
let g:airline_statusline_ontop = 0
let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#enabled = 0
let g:airline#extensions#keymap#enabled = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#flags = 'f'
let g:airline#extensions#ale#enabled = 0
"let g:airline_section_c = '%t%m'
let g:airline_section_z = '%4l/%L%{g:airline_symbols.maxlinenr}% :%3v'
