" Disable changing settings by plugin
let g:polyglot_disabled = ['sensible']
let g:polyglot_disabled += ['csv', 'markdown', 'yaml']
