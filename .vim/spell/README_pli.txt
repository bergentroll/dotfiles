Pali Dictionary for Vim

Based on Digital Pāḷi Dictionary (https://digitalpalidictionary.github.io) and
includes latin (IAST), sinhala, thai, and devanagari scripts.

2024 Anton Karmanov <a.karmanov@inventati.org> CC-0
