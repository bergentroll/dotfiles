" Folding options.
setlocal foldmethod=syntax
setlocal foldlevel=99

" C++ specific options
if (&ft == 'cpp')
    setlocal matchpairs+=<:>
endif
