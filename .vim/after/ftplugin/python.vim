" Do not wrap text
set formatoptions=q

" Fix comments indenting
set smartindent
set autoindent
