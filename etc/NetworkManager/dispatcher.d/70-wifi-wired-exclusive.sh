#!/bin/bash
set -e

function set_wifi {
  out="$(LANG=C nmcli dev)"
  # grep gives 1 on not found
  result="$(echo "$out" | grep 'ethernet' | grep -w 'connected' || true)" 
  echo $result
  if [ -n "$result" ]; then
    echo Setting WiFi off
    nmcli radio wifi off
  else
    echo Setting WiFi on
    nmcli radio wifi on
  fi
}

echo $1 $2
case "$2" in
  up | down) set_wifi ;;
esac
