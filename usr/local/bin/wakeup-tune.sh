#!/bin/sh
set -e

list=$(grep enabled /proc/acpi/wakeup | awk '{ print $1 }' | xargs)
#
for i in ${list}; do
    echo "${i}" > /proc/acpi/wakeup
done

echo "Wakeup disabled for devices: ${list}"
