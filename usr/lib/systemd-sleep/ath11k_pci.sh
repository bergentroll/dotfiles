#!/bin/sh

# https://wiki.archlinux.org/title/Lenovo_ThinkPad_P16s_(AMD)_Gen_2#Hang_on_resume


start_nm() {
  systemctl is-enabled NetworkManager.service && systemctl start NetworkManager.service
}

start_systemd_networkd() {
  systemctl is-enabled systemd-networkd.socket && systemctl start systemd-networkd.socket
  systemctl is-enabled systemd-networkd.service && systemctl start systemd-networkd.service
}

case $1 in
  pre) /usr/bin/systemctl stop NetworkManager.service systemd-networkd.service systemd-networkd.socket; /usr/bin/modprobe -r ath11k_pci ;;
  post) start_nm; start_systemd_networkd; /usr/bin/modprobe ath11k_pci || exit 0 ;;
esac

# vi: syn=sh
