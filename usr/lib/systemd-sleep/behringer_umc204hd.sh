#!/bin/sh

usb_dev='1397:0508'

case $1 in
  pre) ;;
  post) /usr/bin/usbreset "$usb_dev" ;;
esac

# vi: syn=sh
